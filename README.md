Репозиторий содержит решение (2е место на 3м этапе) задачи конкурса **Avito-2016 по Распознаванию категории объявления** (http://www.machinelearning.ru/wiki/index.php?title=Avito-2016-2).

Также доступно видео доклада (https://goo.gl/L1k1FO) с одной из тренировок по машинному обучению.



-------------------------------------
Чтобы воспроизвести решение необходимо иметь файлы с данными доступные в конкурсе.

1) Скачиваем репозиторий, распаковываем, например, в директорию avito_v2

2) В директорию avito_v2 копируем файлы stage1_train.csv, stage1_test.csv, stage3_train.csv, stage3_test.csv

3) Создаём директории avito_v2/train и avito_v2/test в которые распаковываем изображения

4) В файле avito_v2/tensorflow-resnet-master/convert.py выставляем верный путь до директории caffe (version 1.0.0-rc3), например, caffe_root='/home/user/caffe-master/'

5) cd tensorflow-resnet-master6

jupyter notebook feature_extractor_resnet.ipynb

Выполняем все ячейки кода "Cell -> Run All"

6) По завершении cd ..

jupyter notebook cnn_text.ipynb

Выполняем все ячейки кода "Cell -> Run All"

7) jupyter notebook stage1.ipynb

Выполняем все ячейки кода "Cell -> Run All"

8) jupyter notebook stage3.ipynb

Выполняем все ячейки кода "Cell -> Run All"

Итоговый файл 20160926_competition_avito_2016_Pavel_Blinov_Results_v14.csv